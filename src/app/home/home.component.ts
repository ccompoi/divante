import { Component } from '@angular/core';

@Component({
  selector: 'divante-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {}
