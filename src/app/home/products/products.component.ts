import { Component, OnInit } from '@angular/core';

import { Product, ProductService } from './../../services/product.service';
import { SearchService } from './../../services/search.service';

@Component({
  selector: 'divante-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: Product[];
  filteredProducts: Product[];
  searchedValue: string;

  constructor(
    public productService: ProductService,
    public searchService: SearchService,
  ) {}

  private getFilteredItems(searchedValue: string) {
    const parsedSearchedValue = searchedValue.toLowerCase();

    return this.products
      .filter((product) => {
        const parsedTitle = product.title.toLowerCase();
        const parsedDescription = product.description.toLowerCase();

        return parsedTitle.includes(parsedSearchedValue) || parsedDescription.includes(parsedSearchedValue);
      });
  }

  ngOnInit() {
    this.productService.getProducts().subscribe((products) => {
      this.products = products as Product[];
      this.filteredProducts = this.products;
    });

    this.searchService.searchedValue$.subscribe((searchedValue) => {
      this.searchedValue = searchedValue;
      this.filteredProducts = this.getFilteredItems(searchedValue);
    });
  }
}
