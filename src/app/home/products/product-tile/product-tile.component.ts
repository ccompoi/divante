import { Component, Input } from '@angular/core';

@Component({
  selector: 'divante-product-tile',
  templateUrl: './product-tile.component.html',
  styleUrls: ['./product-tile.component.scss']
})
export class ProductTileComponent {
  @Input() id: string;
  @Input() title: string;
  @Input() description: string;
  @Input() image: string;
}
