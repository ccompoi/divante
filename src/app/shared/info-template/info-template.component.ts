import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'divante-info-template',
  templateUrl: './info-template.component.html',
  styleUrls: ['./info-template.component.scss']
})
export class InfoTemplateComponent implements OnInit {
  @Input() title: string;
  @Input() plainText: string;
  @Input() formattedText: any;

  formattedTextInfo: string[];

  ngOnInit() {
    if (this.formattedText) {
      this.formattedTextInfo = this.formattedText.split(',');
    }
  }
}
