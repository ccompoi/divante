import { Component } from '@angular/core';

@Component({
  selector: 'divante-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {
  formattedText = [
    'michalina.sowa.1993@gmail.com',
    '',
    'tel.  881 - 942 - 354',
    'tel. 123 - 456 - 789',
    '',
    'Palma Triq Zakkew',
    'Msida MSD-1661',
    'Malta'
  ];
}
