import { Component } from '@angular/core';

import { SearchService } from './../services/search.service';

@Component({
  selector: 'divante-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  visible = false;

  constructor(
    public searchService: SearchService,
  ) {}

  public toggle() {
    this.visible = !this.visible;
  }

  public onKey(event: any) {
    this.searchService.searchedValue$.next(event.target.value);
  }

  public onFocusLost(event) {
    if (!event.target.value) {
      this.visible = false;
    }
  }
}
