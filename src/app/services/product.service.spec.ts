import { HttpClient } from '@angular/common/http';
import { TestBed, inject } from '@angular/core/testing';
import { Observable, of } from 'rxjs';

import { ProductService, Product } from './product.service';

describe('ProductService', () => {
  let service: ProductService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
          ProductService,
          {
            provide: HttpClient,
            useValue: {
              get: jasmine.createSpy(),
            },
          },
      ],
    });

    service = TestBed.get(ProductService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('.getProducts()', () => {
    it('should call httpClient get method with valid url',
      inject([HttpClient], (httpClient: HttpClient) => {
        const validUrl = '../assets/data/products.json';

        service.getProducts();

        expect(httpClient.get).toHaveBeenCalledWith(validUrl);
    }));
  });
});
