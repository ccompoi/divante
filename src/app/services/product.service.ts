import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Product {
  id: string;
  title: string;
  description: string;
  image: string;
  price: number;
}

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  /**
   * @description address of JSON file from which we get data
   * @todo use real API address
   */
  private productsUrl = '../assets/data/products.json';

  constructor(
    private http: HttpClient,
  ) { }

  /**
   * @description it returns list of products for the shop
   * @returns {Observable<any>} products from JSON file
   */
  public getProducts(): Observable<any> {
    return this.http.get(this.productsUrl);
  }

  /**
   * @description it returns product with id given as parameter
   * @param id id of product that is returned
   * @returns {Observable<Product>} product from JSON file with given id
   */
  public getProduct(id: string): Observable<Product> {
    return this.getProducts().pipe(
      map((products: any[]) => {
        return products.find((product) => product.id === id);
      }),
    );
  }
}
