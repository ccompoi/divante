import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export interface CartItem {
  id: string;
  title: string;
  price: number;
}

@Injectable({
  providedIn: 'root',
})
export class CartService {
  private storageKey = 'cart-items';
  /**
   * cart subject used to handle data propagation between components
   */
  cart$ = new Subject<CartItem[]>();

  /**
   * @description helper method that gets cart data from localStorage and return them
   * @returns {CartItem[]} list of cart items from localStorage
   */
  private getCartItemsFromStorage(): CartItem[] {
    const items = localStorage.getItem(this.storageKey);
    const cartItems: CartItem[] = JSON.parse(items) || [];
    return cartItems;
  }

  /**
   * @description returns all items that are in the cart
   * @returns {CartItem[]} list of items in the cart
   */
  public getItems(): CartItem[] {
    return this.getCartItemsFromStorage();
  }

  /**
   * @description it adds item to the cart
   *              and triggers cart$ to propagate new cart data everywhere
   * @param {CartItem} item cart item that will be added to the cart
   */
  public addItem(item: CartItem): void {
    const cartItems: CartItem[] = this.getCartItemsFromStorage();

    cartItems.push(item);
    localStorage.setItem(this.storageKey, JSON.stringify(cartItems));
    this.cart$.next(cartItems);
  }

  /**
   * @description it removes from the cart item with given id
   *              and triggers cart$ to propagate new cart data everywhere
   * @param {string} id id of product that will be removed
   */
  public removeItem(id: string) {
    const cartItems: CartItem[] = this.getCartItemsFromStorage();
    const filteredItems = cartItems.filter((item) => item.id !== id);

    localStorage.setItem(this.storageKey, JSON.stringify(filteredItems));
    this.cart$.next(filteredItems);
  }

  /**
   * @description returns information if item with given id is in the cart
   * @param {string} id id of product that we want to check
   * @returns {boolean} information if product with given id is in the cart
   */
  public isInCart(id: string) {
    return this.getCartItemsFromStorage().some((item) => item.id === id);
  }
}
