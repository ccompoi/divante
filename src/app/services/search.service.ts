import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  /**
   * @description searched value subject used to handle data propagation between components
   */
  searchedValue$ = new Subject<string>();
}
