import { TestBed } from '@angular/core/testing';

import { CartService, CartItem } from './cart.service';

describe('CartService', () => {
  let service;
  const cartStorageKey = 'cart-items';
  const cartItems: CartItem[] = [
    {
      id: '1',
      title: 'test1',
      price: 11,
    },
    {
      id: '2',
      title: 'test2',
      price: 13,
    },
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });

    service = TestBed.get(CartService);

    // set data in local storage because that's where they came from
    localStorage.setItem(cartStorageKey, JSON.stringify(cartItems));
  });

  afterEach(() => {
    // clean storage after each test to make tests isolated
    localStorage.removeItem(cartStorageKey);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getItems()', () => {
    it ('should return data saved under cart storage key', () => {
      const result = service.getItems();
      expect(result).toEqual(cartItems);
    });
  });

  describe('isInCart()', () => {
    it('should return false when item is not in the cart', () => {
      const result = service.isInCart('123notexisitngid');

      expect(result).toBe(false);
    });

    it ('should return true when item is in the cart', () => {
      const existingItemId = cartItems[0].id;
      const result = service.isInCart(existingItemId);

      expect(result).toBe(true);
    });
  });

  describe('addItem()', () => {
    const item: CartItem = {
      id: '3',
      title: 'title 3',
      price: 31,
    };

    it('should increase the amount of items in the cart by 1', () => {
      const itemsAmountBefore = service.getItems().length;
      service.addItem(item);
      const itemsAmountAfter = service.getItems().length;
      const expectedAmount = itemsAmountBefore + 1;

      expect(expectedAmount).toEqual(itemsAmountAfter);
    });

    it('should add given item to the list of cart items', () => {
      const itemsBefore = service.getItems();
      expect(itemsBefore).not.toContain(item);

      service.addItem(item);

      const itemsAfter = service.getItems();
      expect(itemsAfter).toContain(item);
    });
  });

  describe('removeItem()', () => {
    it('should decrease the amount of items in the cart by 1', () => {
      const removedItemId = '2';

      const itemsAmountBefore = service.getItems().length;
      service.removeItem(removedItemId);
      const itemsAmountAfter = service.getItems().length;
      const expectedAmount = itemsAmountBefore - 1;

      expect(expectedAmount).toEqual(itemsAmountAfter);
    });

    it('should remove item with given id from the list of cart items', () => {
      const removedItemId = '2';

      expect(service.isInCart(removedItemId)).toBe(true);

      service.removeItem(removedItemId);

      expect(service.isInCart(removedItemId)).toBe(false);
    });
  });
});
