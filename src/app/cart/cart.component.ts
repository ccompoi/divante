import { Component, OnInit } from '@angular/core';

import { CartItem, CartService } from './../services/cart.service';

@Component({
  selector: 'divante-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  items: CartItem[];

  constructor(
    private cartService: CartService,
  ) { }

  ngOnInit() {
    this.items = this.cartService.getItems();

    this.cartService.cart$.subscribe((items) => {
      this.items = items;
    });
  }

  public removeItem(id: string) {
    this.cartService.removeItem(id);
  }
}
