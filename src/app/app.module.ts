import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { AppComponent } from './app.component';
import { CartComponent } from './cart/cart.component';
import { ContactComponent } from './contact/contact.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ProductTileComponent } from './home/products/product-tile/product-tile.component';
import { ProductsComponent } from './home/products/products.component';
import { ProductDetailsPageComponent } from './product-details-page/product-details-page.component';
import { ProductDetailsComponent } from './product-details-page/product-details/product-details.component';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InfoTemplateComponent } from './shared/info-template/info-template.component';
import { LoaderComponent } from './shared/loader/loader.component';
import { SearchComponent } from './search/search.component';

const appRoutes: Routes = [
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'home', component: HomeComponent },
  { path: 'products/:id', component: ProductDetailsPageComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContactComponent,
    AboutComponent,
    HomeComponent,
    ProductsComponent,
    ProductTileComponent,
    ProductDetailsComponent,
    CartComponent,
    InfoTemplateComponent,
    ProductDetailsPageComponent,
    PageNotFoundComponent,
    LoaderComponent,
    SearchComponent,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
    ),
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
