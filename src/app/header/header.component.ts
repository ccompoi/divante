import { Component } from '@angular/core';

@Component({
  selector: 'divante-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {}
