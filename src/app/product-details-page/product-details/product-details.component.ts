import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CartService, CartItem } from './../../services/cart.service';
import { Product, ProductService } from './../../services/product.service';

@Component({
  selector: 'divante-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  product: Product;
  isAdded = false;

  constructor(
    private route: ActivatedRoute,
    private cartService: CartService,
    public productService: ProductService,
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.isAdded = this.cartService.isInCart(id);

    this.productService.getProduct(id).subscribe((product) => {
      this.product = product;
    });

    this.cartService.cart$.subscribe(() => {
      this.isAdded = this.cartService.isInCart(this.product.id);
    });
  }

  public addToCart() {
    this.cartService.addItem(this.product as CartItem);
    this.isAdded = this.cartService.isInCart(this.product.id);
  }
}
