import { Component } from '@angular/core';

@Component({
  selector: 'divante-product-details-page',
  templateUrl: './product-details-page.component.html',
  styleUrls: ['./product-details-page.component.scss']
})
export class ProductDetailsPageComponent {}
